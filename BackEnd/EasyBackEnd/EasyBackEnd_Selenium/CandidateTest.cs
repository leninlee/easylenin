﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace EasyBackEnd_Selenium
{
    public class CandidateTest
    {
        public void CreateNewCandidate()
        {
            ChromeOptions options = new ChromeOptions(); ;
            ChromeDriver driver = new ChromeDriver(options);
            try {
                //options = new ChromeOptions();
                //driver = new ChromeDriver(options);

                driver.Navigate().GoToUrl("http://localhost:4200/novocandidato");
                String[] data = { "John", "Ruffalo", "Tom", "Bill", "Ferrel" };
                String[] data2 = { "14073761652", "14073761653", "14073761654", "14073761655", "14073761656" };
                String[] email = { "@gmail.com", "@hotmail.com", "@outlook.com" };
                String[] cities = { "Orlando", "New Jersey", "Poke" };

                IWebElement input_name = driver.FindElementById("name");
                String name = data[new Random().Next(0, data.Length - 1)] + " " + new Random().Next(0, 100).ToString();
                input_name.SendKeys(name);

                IWebElement input_surname = driver.FindElementById("surname");
                name = data[new Random().Next(0, data.Length - 1)] + " " + new Random().Next(0, 100).ToString();
                input_surname.SendKeys(name);

                IWebElement input_phone = driver.FindElementById("phone");
                name = data2[new Random().Next(0, data2.Length - 1)] + " " + new Random().Next(0, 100).ToString();
                input_surname.SendKeys(name);

                IWebElement input_email = driver.FindElementById("email");
                name = data2[new Random().Next(0, data2.Length - 1)] + email[new Random().Next(0, email.Length - 1)];
                input_email.SendKeys(name);

                IWebElement input_skype = driver.FindElementById("skype");
                name = data2[new Random().Next(0, data2.Length - 1)] + email[new Random().Next(0, email.Length - 1)];
                input_skype.SendKeys(name);

                IWebElement input_linkedin = driver.FindElementById("linkedin");
                name = data2[new Random().Next(0, data2.Length - 1)] + email[new Random().Next(0, email.Length - 1)];
                input_linkedin.SendKeys(name);

                IWebElement input_city = driver.FindElementById("city");
                name = cities[new Random().Next(0, cities.Length - 1)];
                input_city.SendKeys(name);

                IWebElement input_state = driver.FindElementById("states");
                name = cities[new Random().Next(0, cities.Length - 1)];
                input_state.SendKeys(name);

                IWebElement input_houvalue = driver.FindElementById("hourvalue");
                name = new Random().Next(0, 20).ToString();
                input_houvalue.SendKeys(name);

                IWebElement input_coverletter = driver.FindElementById("coverletter");
                name = new Random().Next(0, 100).ToString();
                input_coverletter.SendKeys(name);

                IWebElement input_send = driver.FindElementById("send");
                input_send.Click();
                Thread.Sleep(2000);

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (driver != null)
                    driver.Quit();
            }

            

        }

    }
}
