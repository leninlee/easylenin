﻿Create table Jobs(
  Id        int not null identity primary key,
  title     varchar(100),
  container text
)

Create table CandidateJob(
    Id          int not null identity primary key,
    IdCandidate int not null,
    IdJob       int not null,
    foreign key(IdCandidate) references Candidate(Id),
    foreign key(IdJob)       references Jobs(Id)
)