﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBackEnd_Domain.Entity;
using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_Domain.Log;
using EasyBackEnd_Domain.Repository;
using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateJobController : ControllerBase
    {

        ICandidateJobRepository candidateJobRepository;

        public CandidateJobController(ICandidateJobRepository candidateJobRepository)
        {
            this.candidateJobRepository = candidateJobRepository;
        }

        // GET: api/CandidateJob
        [HttpGet]
        public List<CandidateJobDetail> GetCandidateJob()
        {
            List<CandidateJobDetail> result = new List<CandidateJobDetail>();
            try {
                result = candidateJobRepository.findAllDetail();
            }
            catch(Exception ex) {
                LogData.Log(ex.Message);
            }
            return result;
        }

        // GET: api/CandidateJob/5
        [HttpGet("{id}")]
        public CandidateJob GetCandidateJobItem(int id)
        {
            CandidateJob candidateJob = new CandidateJob();
            try {
                candidateJob = candidateJobRepository.find(new CandidateJob() { Id = id }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return candidateJob;
        }

        // POST: api/CandidateJob
        [HttpPost]
        public void Post([FromBody] CandidateJob value)
        {
            try {
                candidateJobRepository.saveUpdate(value);
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // PUT: api/CandidateJob/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] CandidateJob value)
        {
            try {
                value.Id = id;
                candidateJobRepository.saveUpdate(value);
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try {
                candidateJobRepository.remove(new CandidateJob() { Id = id });
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

    }
}
