﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_Domain.Log;
using EasyBackEnd_Domain.Repository;
using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateController : ControllerBase
    {

        ICandidateRepository candidateRepository;

        public CandidateController(ICandidateRepository candidateRepository)
        {
            this.candidateRepository = candidateRepository;
        }

        // GET: api/Candidate
        [HttpGet]
        public List<Candidate> Get()
        {
            List<Candidate> result = new List<Candidate>();
            try {
                result = candidateRepository.findAll();
            }
            catch(Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        // GET: api/Candidate/5
        [HttpGet("{id}", Name = "Get")]
        public List<Candidate> Get(string id)
        {
            List<Candidate> candidate = new List<Candidate>();
            try {
                candidate = candidateRepository.find(new Candidate() { name = id });
            }
            catch(Exception ex) {
                LogData.Log(ex.Message);
            }
            
            return candidate;
        }

        // POST: api/Candidate
        [HttpPost]
        public void Post([FromBody] Candidate value)
        {
            try {
                candidateRepository.saveUpdate(value);
            }
            catch(Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // PUT: api/Candidate/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Candidate value)
        {
            try {
                value.Id = id;
                candidateRepository.saveUpdate(value);
            }
            catch(Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try {
                candidateRepository.remove(new Candidate() { Id = id });
            }
            catch(Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

    }
}
