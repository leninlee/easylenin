﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_Domain.Log;
using EasyBackEnd_Domain.Repository;
using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {

        IJobsRepository jobsRepository;

        public JobsController(IJobsRepository jobsRepository)
        {
            this.jobsRepository = jobsRepository;
        }

        // GET: api/Jobs
        [HttpGet]
        public List<Jobs> Get()
        {
            List<Jobs> result = new List<Jobs>(); 
            try {
                result = jobsRepository.findAll();
            }
            catch(Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        // GET: api/Jobs/5
        [HttpGet("{id}")]
        public Jobs Get(int id)
        {
            Jobs jobs = new Jobs();
            try
            {
                jobs = jobsRepository.find(new Jobs() { Id = id }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return jobs;
        }

        // POST: api/Jobs
        [HttpPost]
        public void Post([FromBody] Jobs value)
        {
            try
            {
                jobsRepository.saveUpdate(value);
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // PUT: api/Jobs/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Jobs value)
        {
            try
            {
                value.Id = id;
                jobsRepository.saveUpdate(value);
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                jobsRepository.remove(new Jobs() { Id = id });
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            
        }

    }
}
