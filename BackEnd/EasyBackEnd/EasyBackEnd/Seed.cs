﻿using EasyBackEnd_Domain.Repository;
using EasyBackEnd_Domain.ValueObject;
using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBackEnd
{
    public class Seed
    {
        CandidateRepository candidateRepository;
        JobsRepository jobsRepository;
        CandidateJobRepository candidateJobRepository;

        public Seed()
        {
            candidateRepository = new CandidateRepository();
            jobsRepository = new JobsRepository();
            candidateJobRepository = new CandidateJobRepository();
        }

        public void LoadData()
        {
            //load candidates:
            LoadCandidates();
            //load jobs:
            LoadJobs();
            //load association between candidate and job
            LoadCandidateJob();

        }

        public void LoadJobs()
        {
            int jobRecords = 20;
            List<Jobs> jobs = new List<Jobs>();
            string[] randomText = { "Software Developer", "Front-end Developer", "Back-End Developer", "Web Designer", "Database Administrator" };
            string[] randomContainer = { "Requisitos : .Net Core, Node js", "Requisitos: Angular, React, HTML, CSS", "Soft Skills: Atenção a detalhes, noções de web designer" };
            int rndText = 0;
            int rndContainer = 0;

            for (int i = 0; i < jobRecords; i++)
            {
                rndText = (rndText < randomText.Length - 1 ? rndText : 0);
                rndContainer = (rndContainer < randomContainer.Length - 1 ? rndContainer : 0);

                rndText = new Random().Next(rndText, randomText.Length - 1);
                rndContainer = new Random().Next(rndContainer, randomContainer.Length - 1);

                Jobs job = new Jobs();
                job.active = FixedValues.Active.A.ToString();
                job.container = randomContainer[rndContainer];
                job.title = randomText[rndText];
                jobs.Add(job);
            }

            jobsRepository.saveRange(jobs);
        }

        public void LoadCandidates()
        {
            int candidateRecords = 20;
            List<Candidate> candidates = new List<Candidate>();
            string[] randomText = { "John", "Mary", "Constanza", "Mark", "Ana", "Pedro", "Marcos" };
            string[] randomMail = { "@hotmail.com", "@gmail.com", "@zip.com.br" };
            int rndText = 0;
            int rndMail = 0;

            for (int i = 0; i < candidateRecords; i++)
            {
                rndText = (rndText < randomText.Length - 1 ? rndText : 0);
                rndMail = (rndMail < randomMail.Length - 1 ? rndMail : 0);

                rndText = new Random().Next(rndText, randomText.Length - 1);
                rndMail = new Random().Next(rndMail, randomMail.Length - 1);
                

                Candidate candidate = new Candidate();
                candidate.active = FixedValues.Active.A.ToString();
                candidate.coverletter = "None";
                candidate.email = randomText[rndText] + randomMail[rndMail];
                candidate.name = randomText[rndText];
                candidate.phone = new Random().Next(0, 999999999).ToString();
                rndText = new Random().Next(rndText, randomText.Length - 1);
                candidate.surname = randomText[rndText] + new Random().Next(0, 999999999).ToString();
                candidates.Add(candidate);

                rndText += 1;
                rndMail += 1;
            }

            candidateRepository.saveRange(candidates);
        }

        public void LoadCandidateJob()
        {
            List<CandidateJob> candidateJobs = new List<CandidateJob>();
            List<Candidate> candidates = candidateRepository.findAll();
            List<Jobs> jobs = jobsRepository.findAll();
            int candidateJobAssociation = 20;
            int candNumber = 0;
            int jobNumber = 0;
            for (int i = 0;i < candidateJobAssociation; i++)
            {
                candNumber = (candNumber < candidates.Count() - 1 ? candNumber : 0);
                jobNumber = (jobNumber < jobs.Count() - 1 ? jobNumber : 0);

                candNumber = new Random().Next(candNumber, candidates.Count()-1);
                jobNumber = new Random().Next(jobNumber, jobs.Count() - 1);
                CandidateJob candidateJob = new CandidateJob();
                candidateJob.IdCandidate = candidates[candNumber].Id;
                candidateJob.IdJob = jobs[jobNumber].Id;
                candidateJobs.Add(candidateJob);
            }

            //insert data:
            candidateJobRepository.saveRange(candidateJobs);

        }

    }

}
