﻿using EasyBackEnd_Domain.ValueObject;
using EasyBackEnd_InfraStructure.FileManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_Domain.Log
{
    public class LogData
    {
        public static void Log(String data)
        {
            new FileManagement().write(data, FixedValues.fileLog);
        }
    }
}
