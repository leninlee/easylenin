﻿using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_InfraStructure.DataAccess;
using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EasyBackEnd_Domain.ValueObject;
using EasyBackEnd_Domain.Log;

namespace EasyBackEnd_Domain.Repository
{
    public class CandidateRepository : ICandidateRepository
    {
        public List<Candidate> find(Candidate entry)
        {
            List<Candidate> result = new List<Candidate>();
            try { 
                using(var context = new CandidateContext())
                {
                    result = context.Candidate.Where(w => w.Equals(entry)).ToList();
                    if(result.Count() == 0 && entry.name != null)
                    {
                        result = context.Candidate.Where(w => w.name.ToLower().Contains(entry.name.ToLower())  ).ToList();
                    }
                    if (result.Count() == 0 && entry.name != null && Char.IsDigit(entry.name.ToCharArray()[0]) )
                    {
                        int id = Int32.Parse(entry.name);
                        result = context.Candidate.Where(w => w.Id == id).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public List<Candidate> findAll()
        {
            List<Candidate> result = new List<Candidate>();
            try
            {
                using (var context = new CandidateContext())
                {
                    string active = FixedValues.Active.A.ToString();
                    result = context.Candidate.Where(w => w.active == active).ToList();
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remoteRange(List<Candidate> entry)
        {
            bool result = false;
            try
            {
                entry.ForEach(item =>
                {
                    result = remove(item);
                    if (!result)
                    {
                        throw new Exception("Failure on remove item!");
                    }
                });
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remove(Candidate entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    var rem = context.Candidate.Where(w => w.Id == entry.Id).FirstOrDefault();
                    context.Candidate.Attach(rem);
                    rem.active = FixedValues.Active.I.ToString();
                    
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveRange(List<Candidate> entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    context.AddRange(entry);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveUpdate(Candidate entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    if(entry.Id != 0)
                    {
                        var rem = context.Candidate.Where(w => w.Id == entry.Id).FirstOrDefault();
                        context.Candidate.Attach(rem);

                        rem.active = entry.active;
                        rem.coverletter = entry.coverletter;
                        rem.email = entry.email;
                        rem.name = entry.name;
                        rem.phone = entry.phone;
                        rem.surname = entry.surname;

                        context.SaveChanges();
                    }
                    else
                    {
                        context.Add(entry);
                        context.SaveChanges();
                    }
                    
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

    }

}
