﻿using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_InfraStructure.DataAccess;
using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EasyBackEnd_Domain.ValueObject;
using EasyBackEnd_Domain.Entity;
using EasyBackEnd_Domain.Log;

namespace EasyBackEnd_Domain.Repository
{
    public class CandidateJobRepository : ICandidateJobRepository
    {
        string treatMessage = "Failure on remove item!";

        public List<CandidateJob> find(CandidateJob entry)
        {
            List<CandidateJob> result = new List<CandidateJob>();
            try
            {
                using (var context = new CandidateContext())
                {
                    result = context.CandidateJob.Where(w => w.Equals(entry)).ToList();
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public List<CandidateJob> findAll()
        {
            List<CandidateJob> result = new List<CandidateJob>();
            try
            {
                using (var context = new CandidateContext())
                {
                    result = context.CandidateJob.ToList();
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public List<CandidateJobDetail> findAllDetail()
        {
            List<CandidateJob> result_intermediate = new List<CandidateJob>();
            List<Candidate> candidates = new List<Candidate>();
            List<Jobs> jobs = new List<Jobs>();
            List<CandidateJobDetail> result = new List<CandidateJobDetail>();

            try
            {
                using (var context = new CandidateContext())
                {
                    result_intermediate = context.CandidateJob.ToList();
                    var arr_candidates = result_intermediate.Select(s => s.IdCandidate).ToArray();
                    var arr_jobs = result_intermediate.Select(s => s.IdJob).ToArray();

                    candidates = context.Candidate.Where(w => arr_candidates.Contains(w.Id)).ToList();
                    jobs = context.Jobs.Where(w => arr_jobs.Contains(w.Id)).ToList();

                    result = TreatData(result_intermediate, candidates, jobs);
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remoteRange(List<CandidateJob> entry)
        {
            bool result = false;
            try
            {
                entry.ForEach(item =>
                {
                    result = remove(item);
                    if (!result)
                    {
                        throw new Exception(treatMessage);
                    }
                });
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remove(CandidateJob entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    var rem = context.CandidateJob.Where(w => w.Id == entry.Id).FirstOrDefault();
                    context.CandidateJob.Remove(rem);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveRange(List<CandidateJob> entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    context.AddRange(entry);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveUpdate(CandidateJob entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    if (entry.Id != 0)
                    {
                        var rem = context.CandidateJob.Where(w => w.Id == entry.Id).FirstOrDefault();
                        context.CandidateJob.Attach(rem);

                        rem.IdCandidate = entry.IdCandidate;
                        rem.IdJob = entry.IdJob;

                        context.SaveChanges();
                    }
                    else
                    {
                        context.Add(entry);
                        context.SaveChanges();
                    }

                    result = true;
                }
            }
            catch (Exception)
            {
                //implement after
            }
            return result;
        }

        private List<CandidateJobDetail> TreatData(List<CandidateJob> candidateJobs
            , List<Candidate> candidates, List<Jobs> jobs )
        {
            List<CandidateJobDetail> result = new List<CandidateJobDetail>();
            candidateJobs.ForEach(cJob =>
            {
                CandidateJobDetail candidateJobDetail = new CandidateJobDetail();
                candidateJobDetail.Id = cJob.Id;
                candidateJobDetail.IdCandidate = cJob.IdCandidate;
                candidateJobDetail.IdJob = cJob.IdJob;
                candidateJobDetail.NameCandidate = candidates.Where(w => w.Id == cJob.IdCandidate).First().name;
                candidateJobDetail.NameJob = jobs.Where(w => w.Id == cJob.IdJob).First().title;
                result.Add(candidateJobDetail);
            });
            return result;
        }

    }

}
