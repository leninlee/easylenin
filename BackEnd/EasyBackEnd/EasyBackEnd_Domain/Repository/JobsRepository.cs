﻿using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_InfraStructure.DataAccess;
using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EasyBackEnd_Domain.ValueObject;
using EasyBackEnd_Domain.Log;

namespace EasyBackEnd_Domain.Repository
{
    public class JobsRepository : IJobsRepository
    {
        string message = "Failure on remove item!";

        public List<Jobs> find(Jobs entry)
        {
            List<Jobs> result = new List<Jobs>();
            try
            {
                using (var context = new CandidateContext())
                {
                    result = context.Jobs.Where(w => w.Equals(entry)).ToList();
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public List<Jobs> findAll()
        {
            List<Jobs> result = new List<Jobs>();
            try
            {
                using (var context = new CandidateContext())
                {
                    string active = FixedValues.Active.A.ToString();
                    result = context.Jobs.Where(w => w.active == active).ToList();
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remoteRange(List<Jobs> entry)
        {
            bool result = false;
            try
            {
                entry.ForEach(item =>
                {
                    result = remove(item);
                    if (!result)
                    {
                        throw new Exception(message);
                    }
                });
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool remove(Jobs entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    var rem = context.Jobs.Where(w => w.Id == entry.Id).FirstOrDefault();
                    context.Jobs.Attach(rem);
                    rem.active = FixedValues.Active.I.ToString();

                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveRange(List<Jobs> entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    context.AddRange(entry);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

        public bool saveUpdate(Jobs entry)
        {
            bool result = false;
            try
            {
                using (var context = new CandidateContext())
                {
                    if (entry.Id != 0)
                    {
                        var rem = context.Jobs.Where(w => w.Id == entry.Id).FirstOrDefault();
                        context.Jobs.Attach(rem);

                        rem.active = entry.active;
                        rem.container = entry.container;
                        rem.title = entry.title;

                        context.SaveChanges();
                    }
                    else
                    {
                        context.Add(entry);
                        context.SaveChanges();
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogData.Log(ex.Message);
            }
            return result;
        }

    }
}
