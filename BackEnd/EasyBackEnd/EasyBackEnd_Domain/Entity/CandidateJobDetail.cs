﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_Domain.Entity
{
    public class CandidateJobDetail
    {
        public int Id { get; set; }
        public int IdCandidate { get; set; }
        public int IdJob { get; set; }

        public String NameCandidate { get; set; }

        public String NameJob { get; set; }

    }

}
