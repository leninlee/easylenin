﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_Domain.Interfaces
{
    public interface IRepository<T>
    {
        List<T> findAll();
        List<T> find(T entry);
        bool saveUpdate(T entry);

        bool saveRange(List<T> entry);

        bool remove(T entry);
        bool remoteRange(List<T> entry);
    }

}
