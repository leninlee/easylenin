﻿using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_Domain.Interfaces
{
    public interface IJobsRepository:IRepository<Jobs>
    {

    }

}
