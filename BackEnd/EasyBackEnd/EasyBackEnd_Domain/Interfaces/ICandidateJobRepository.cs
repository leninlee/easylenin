﻿using EasyBackEnd_Domain.Entity;
using EasyBackEnd_InfraStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_Domain.Interfaces
{
    public interface ICandidateJobRepository: IRepository<CandidateJob>
    {
        List<CandidateJobDetail> findAllDetail();
    }

}
