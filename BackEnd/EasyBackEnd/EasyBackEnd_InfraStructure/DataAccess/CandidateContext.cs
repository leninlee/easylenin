﻿using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Data.Entity;
using System.Text;
using EasyBackEnd_InfraStructure.FileManagement;

namespace EasyBackEnd_InfraStructure.DataAccess
{
    public class CandidateContext : DbContext
    {
        String myConnection = String.Empty;
        String origin = "ConnectionData.txt";

        public CandidateContext()
        {
            myConnection = new FileManagement.FileManagement().read(origin);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            myConnection = new FileManagement.FileManagement().read(origin);
            optionsBuilder.UseSqlServer(myConnection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Candidate> Candidate { get; set; }
        public DbSet<Jobs> Jobs { get; set; }
        public DbSet<CandidateJob> CandidateJob { get; set; }

    }
}
