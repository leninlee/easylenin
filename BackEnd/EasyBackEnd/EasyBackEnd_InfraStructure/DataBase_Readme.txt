﻿-- Estruturas das tabelas:
//Candidato:
Create table Candidate(
  Id            int not null identity primary key,
  [name]        varchar(50) not null,
  [surname]     varchar(100) not null,
  [phone]       varchar(25) ,
  [email]       varchar(50) ,
  [coverletter] text,
  active        char(1)
)
go
Alter table Candidate add skype			varchar(200)
Alter table Candidate add linkedin		varchar(200)
Alter table Candidate add city			varchar(200)
Alter table Candidate add states		varchar(200)
Alter table Candidate add willingness   char(1)
Alter table Candidate add hourly		varchar(20)
//vagas
Create table Jobs(
  Id        int not null identity primary key,
  title     varchar(100),
  container text
)
Alter table Jobs add active char(1)
//candidato vs vagas
Create table CandidateJob(
    Id          int not null identity primary key,
    IdCandidate int not null,
    IdJob       int not null,
    foreign key(IdCandidate) references Candidate(Id),
    foreign key(IdJob)       references Jobs(Id)
)

