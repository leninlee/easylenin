﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EasyBackEnd_InfraStructure.FileManagement
{
    public class FileManagement
    {
        public void write(String container,String fileName)
        {
            try
            {
                using(StreamWriter writer = new StreamWriter(fileName))
                {
                    writer.Write(container);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public String read(String fileName)
        {
            String result = String.Empty;
            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    result = reader.ReadToEnd();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

    }
}
