﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_InfraStructure.Repositories
{
    public class CandidateJob
    {
        public int Id { get; set; }
        public int IdCandidate { get; set; }
        public int IdJob { get; set; }

    }

}
