﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_InfraStructure.Repositories
{
    public class Candidate
    {
        public int Id { get; set; } 
        public String name { get; set; }
        public String surname { get; set; }
        public String phone { get; set; }
        public String email { get; set; }
        public String coverletter { get; set; }

        public String active { get; set; }

        public String skype { get; set; }
        public String linkedin { get; set; }
        public String city { get; set; }
        public String states { get; set; }
        public String willingness { get; set; }
        public String hourly { get; set; }


    }

}
