﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBackEnd_InfraStructure.Repositories
{
    public class Jobs
    {
        public int Id { get; set; }
        public String title { get; set; }

        public String container { get; set; }

        public String active { get; set; }

    }

}
