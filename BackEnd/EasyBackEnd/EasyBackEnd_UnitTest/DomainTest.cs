﻿using EasyBackEnd_Domain.Entity;
using EasyBackEnd_Domain.Interfaces;
using EasyBackEnd_Domain.Repository;
using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EasyBackEnd_UnitTest
{
    [TestClass]
    public class DomainTest
    {
        ICandidateJobRepository candidateJobRepository;
        IJobsRepository jobsRepository;
        ICandidateRepository candidateRepository;

        public DomainTest()
        {
            candidateJobRepository = new CandidateJobRepository();
            jobsRepository = new JobsRepository();
            candidateRepository = new CandidateRepository();
        }

        [TestMethod]
        public void findCandidateJobRepository()
        {
            var candidateJobItem = candidateJobRepository.findAll().First();
            candidateJobRepository.find(candidateJobItem);
        }

        [TestMethod]
        public void findAllCandidateJob()
        {
           candidateJobRepository.findAll();
        }

        [TestMethod]
        public void findAllDetailCandidateJob()
        {
            candidateJobRepository.findAllDetail();
        }

        [TestMethod]
        public void remoteRangeCandidateJob()
        {
            List<CandidateJob> entry = new List<CandidateJob>();
            entry.Add(new CandidateJob() { IdCandidate = 1, IdJob = 1 });
            candidateJobRepository.remoteRange(entry);
        }

        [TestMethod]
        public void  removeCandidateJob()
        {
            CandidateJob entry = new CandidateJob();
            entry.IdCandidate = 1;entry.IdJob = 1;
            candidateJobRepository.remove(entry);
        }

        [TestMethod]
        public void saveRangeCandidateJob()
        {
            List<CandidateJob> entry = new List<CandidateJob>();
            int IdCandidate = candidateRepository.findAll().First().Id;
            int IdJob = jobsRepository.findAll().First().Id;
            entry.Add(new CandidateJob() { IdCandidate = IdCandidate, IdJob = IdJob });
            candidateJobRepository.saveRange(entry);
        }

        [TestMethod]
        public void saveUpdateCandidateJob()
        {
            int IdCandidate = candidateRepository.findAll().First().Id;
            int IdJob = jobsRepository.findAll().First().Id;
            CandidateJob entry = new CandidateJob() { IdCandidate = IdCandidate, IdJob = IdJob };
            candidateJobRepository.saveUpdate(entry);
        }

        [TestMethod]
        public void findAllJobs()
        {
            jobsRepository.findAll();
        }

        [TestMethod]
        public void findJobItem()
        {
            Jobs entry = new Jobs() { Id = 1 };
            jobsRepository.find(entry);
        }

        [TestMethod]
        public void saveUpdateJobs()
        {
            Jobs entry = new Jobs() { title = "Random Job", container = "My Description", active = "A" };
            jobsRepository.saveUpdate(entry);
        }

        [TestMethod]
        public void saveRange()
        {
            Jobs entry = new Jobs() { title = "Random Job 2", container = "My Description", active = "A" };
            List<Jobs> list = new List<Jobs>();
            list.Add(entry);
            jobsRepository.saveRange(list);
        }

        [TestMethod]
        public void removeJob()
        {
            Jobs entry = new Jobs() { Id = 1 };
            jobsRepository.remove(entry);
        }

        [TestMethod]
        public void remoteRangeJobs()
        {
            List<Jobs> entry = new List<Jobs>();
            Jobs job = new Jobs() { Id = 1 };
            entry.Add(job);
            jobsRepository.remoteRange(entry);
        }

        [TestMethod]
        public void findAllCandidates()
        {
            candidateRepository.findAll();
        }

        [TestMethod]
        public void findCandidate()
        {
            Candidate entry = new Candidate() { Id = 1 };
            candidateRepository.find(entry);
        }

        [TestMethod]
        public void saveUpdate()
        {
            Candidate entry = new Candidate() {active = "A",city="Brussels"
                ,coverletter="None",email="xxx@hotmail.com",hourly = "20"
                ,linkedin= "https://www.linkedin.com/in/lenin-aguiar-758502a/"
                ,name="Example",phone="111111111",skype="leninlee",states="Brussels"
                ,surname="My Surname",willingness="1"
            };
            candidateRepository.saveUpdate(entry);
        }

        [TestMethod]
        public void saveRangeCandidate()
        {
            List<Candidate> list = new List<Candidate>();
            Candidate entry = new Candidate()
            {
                active = "A",
                city = "Brussels",
                coverletter = "None",
                email = "xxx@hotmail.com",
                hourly = "20",
                linkedin = "https://www.linkedin.com/in/lenin-aguiar-758502a/",
                name = "Example",
                phone = "111111111",
                skype = "leninlee",
                states = "Brussels",
                surname = "My Surname",
                willingness = "1"
            };
            list.Add(entry);

            candidateRepository.saveRange(list);
        }

        [TestMethod]
        public void removeCandidate()
        {
            Candidate entry = new Candidate() {Id = 1 };
            candidateRepository.remove(entry);
        }

        [TestMethod]
        public void remoteRange()
        {
            List<Candidate> list = new List<Candidate>();
            Candidate entry = new Candidate() { Id = 1 };
            list.Add(entry);
            candidateRepository.remoteRange(list);
        }

    }
}
