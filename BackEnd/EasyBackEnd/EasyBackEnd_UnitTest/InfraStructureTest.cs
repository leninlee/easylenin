using EasyBackEnd_InfraStructure.DataAccess;
using EasyBackEnd_InfraStructure.FileManagement;
using EasyBackEnd_InfraStructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyBackEnd_UnitTest
{
    [TestClass]
    public class InfraStructureTest
    {
        //JobsRepository jobsRepository;

        [TestMethod]
        public void TestDataBaseConnection()
        {
            using(var context = new CandidateContext())
            {
                var candidates = context.Candidate.ToList();
            }
        }

        [TestMethod]
        public void TestInsertCandidate()
        {
            using (var context = new CandidateContext())
            {
                //var candidates = context.Candidate.ToList();
                Candidate candidate = new Candidate();
                candidate.active = "A";
                candidate.coverletter = "None";
                candidate.email = "xxx@gmail.com";
                candidate.name = "John";
                candidate.surname = "Nash";
                candidate.phone = "1407777777";
                context.Candidate.Add(candidate);
                context.SaveChanges();
            }
        }

        [TestMethod]
        public void TestInsertJob()
        {

            string[] randomText = { "Software Developer", "Front-end Developer", "Back-End Developer", "Web Designer", "Database Administrator" };
            string[] randomContainer = { "Requisitos : .Net Core, Node js", "Requisitos: Angular, React, HTML, CSS", "Soft Skills: Aten��o a detalhes, no��es de web designer" };
            int rndText = 0;
            int rndContainer = 0;

                rndText = (rndText < randomText.Length - 1 ? rndText : 0);
                rndContainer = (rndContainer < randomContainer.Length - 1 ? rndContainer : 0);

                rndText = new Random().Next(rndText, randomText.Length - 1);
                rndContainer = new Random().Next(rndContainer, randomContainer.Length - 1);

                Jobs job = new Jobs();
                job.active = "A";
                job.container = randomContainer[rndContainer];
                job.title = randomText[rndText] + " " + new Random().Next(0, 100); ;

                using (var context = new CandidateContext())
                {
                    context.Jobs.Add(job);
                    context.SaveChanges();
                }

        }

        [TestMethod]
        public void TestInsertCandidateJob()
        {
            CandidateJob candidateJob = new CandidateJob();
            Candidate candidates = new Candidate();
            Jobs jobs = new Jobs();

            using (var context = new CandidateContext())
            {
                candidates = context.Candidate.First();
                jobs = context.Jobs.First();
                candidateJob.IdCandidate = candidates.Id;
                candidateJob.IdJob = jobs.Id;
                context.Add(candidateJob);
                context.SaveChanges();
            }

        }

        [TestMethod]
        public void TryWriteFile()
        {
            new FileManagement().write("Test File", "NewTestFile.txt");
        }

        [TestMethod]
        public void TryReadFile()
        {
            String result = new FileManagement().read("NewTestFile.txt");
        }

    }
}
