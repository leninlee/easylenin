import { Component, OnInit } from '@angular/core';
import { Candidate } from '../../models/candidatomodel.model';
import { CandidatoserviceService } from '../../servicos/candidatoservice.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-novocandidato',
  templateUrl: './novocandidato.component.html',
  styleUrls: ['./novocandidato.component.css']
})
export class NovocandidatoComponent implements OnInit {

  public name:any;
  surname:any;
  phone:any;
  email:any;
  linkedin:any;
  coverletter:any;
  skype:string;
  city:string;
  states:string;
  willingness:string = "1";
  hourly:string;
  Id:number;
  public form:Candidate;

  constructor(private candidateService:CandidatoserviceService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.Id = this.activatedRoute.snapshot.params['id'];
    if(this.Id > 0){
      this.loadCandidate();
    }
  }

  disponibility(event:any,code:any){
    if(event.target.checked){
      this.willingness = code;
      console.log("This is checked")
    } else {
      console.log("unchecked");
    }
  }

  loadCandidate(){
    this.candidateService.getById(this.Id.toString()).subscribe(data => {
      //console.log(data);
      console.log('Get data');
      this.form = <Candidate> data[0];
      this.name = this.form.name;
      this.surname = this.form.surname;
      //this.Id = this.form.Id;
      this.email = this.form.email;
      this.coverletter = this.form.coverletter;
      this.phone = this.form.phone;
      //new fields:
      this.skype        = this.form.skype;
      this.linkedin     = this.form.linkedin;
      this.city         = this.form.city;
      this.states       = this.form.states;
      this.willingness  = this.form.willingness;
      this.hourly       = this.form.hourly;
      //console.log(this.form.Id);
      console.log(this.Id);
      console.log(this.surname);

    });
  }

  saveCandidate(){
    console.log('saving candidate!');
    console.log(this.name);
    console.log(this.surname);
    this.form = new Candidate();
    this.form.name = this.name;
    this.form.active = "A";
    this.form.coverletter = this.coverletter;
    this.form.email = this.email;
    this.form.phone = this.phone;
    this.form.surname = this.surname;
    console.log(this.Id);
    //this.form.Id = <number> this.Id;
    //new fields:
    this.form.skype       = this.skype;
    this.form.linkedin    = this.linkedin;
    this.form.city        = this.city;
    this.form.states      = this.states;
    this.form.willingness = this.willingness;
    this.form.hourly      = this.hourly;

    if(this.Id==null){
      this.Id = 0;
    }

    console.log(this.form);
    this.candidateService.saveCandidate(this.form,this.Id).subscribe(data => {
      //console.log(data);
      console.log('Saved data');
    });
  }

  queryCandidates(){
    window.location.href='/consulta';
  }

}
