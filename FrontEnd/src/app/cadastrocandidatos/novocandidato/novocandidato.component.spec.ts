import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovocandidatoComponent } from './novocandidato.component';

describe('NovocandidatoComponent', () => {
  let component: NovocandidatoComponent;
  let fixture: ComponentFixture<NovocandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovocandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovocandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
