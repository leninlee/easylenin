import { Component, OnInit } from '@angular/core';
import { CandidatoserviceService } from '../../servicos/candidatoservice.service';

@Component({
  selector: 'app-consultacandidato',
  templateUrl: './consultacandidato.component.html',
  styleUrls: ['./consultacandidato.component.css']
})
export class ConsultacandidatoComponent implements OnInit {

  //candidates:any = [
  //  {name:'Lenin',surname:'Aguiar',phone:'14073761652',email:'leninlee@hotmail.com'},
  //  {name:'Lili',surname:'Aguiar',phone:'14073761653',email:'leninlee2@hotmail.com'}
  //];
  candidates:any = [];
  queryentry:string;

  //private candidateService:CandidatoserviceService
  constructor(private candidateService:CandidatoserviceService) { }

  ngOnInit(): void {
    console.log('started!');
    this.loadCandidates();
  }

  filterCandidates(){
    console.log('filterCandidates-executed');
    console.log(this.queryentry);
    let localname:string = this.queryentry;
    //this.candidates = this.candidateService.getAll();
    this.candidateService.getById(localname).subscribe(data => {
      //console.log(data);
      console.log('executed');
      this.candidates = data;
      //console.log(data);
    });
  }

  loadCandidates(){
    console.log('loadCandidates-executed');
    //this.candidates = this.candidateService.getAll();
    this.candidateService.getAll().subscribe(data => {
      //console.log(data);
      this.candidates = data;
      //console.log(data);
    });
  }

  editCandidates(event:any,item:any){
    console.log('Edit data 2');
    console.log(item);
    window.location.href="novocandidato/" + item.id;
  }

  removeCandidates(event:any,item:any){
    console.log('Edit data 2');
    console.log(item);
    if(window.confirm('Confirma exclusão de registro?')){
      console.log('Confirma exclusao de registro');
      this.candidateService.removeCandidate(item.id).subscribe(data => {
        //this.candidates = data;
        alert('Registro excluído com sucesso!');
        window.location.href="/consulta";
      });
    }
    
  }

  newCandidate(){
    window.location.href="/novocandidato";
  }

}
