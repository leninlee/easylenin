import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultacandidatoComponent } from './consultacandidato.component';

describe('ConsultacandidatoComponent', () => {
  let component: ConsultacandidatoComponent;
  let fixture: ComponentFixture<ConsultacandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultacandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultacandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
