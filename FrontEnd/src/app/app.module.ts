import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NovocandidatoComponent } from './cadastrocandidatos/novocandidato/novocandidato.component';
import { ConsultacandidatoComponent } from './consultacandidato/consultacandidato/consultacandidato.component';
import { HomeComponent } from './home/home/home.component';
import { CandidatoserviceService } from './servicos/candidatoservice.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ConsultavagaComponent } from './vagas/consultavaga/consultavaga/consultavaga.component';
import { NovavagaComponent } from './vagas/novavaga/novavaga/novavaga.component';
import { ConsultavagacandidatoComponent } from './vagacandidato/consultavagacandidato/consultavagacandidato/consultavagacandidato.component';
import { NovavagacandidatoComponent } from './vagacandidato/novavagacandidato/novavagacandidato/novavagacandidato.component';

@NgModule({
  declarations: [
    AppComponent,
    NovocandidatoComponent,
    ConsultacandidatoComponent,
    HomeComponent,
    ConsultavagaComponent,
    NovavagaComponent,
    ConsultavagacandidatoComponent,
    NovavagacandidatoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CandidatoserviceService,HttpClientModule,HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
