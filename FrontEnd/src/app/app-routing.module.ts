import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NovocandidatoComponent } from './cadastrocandidatos/novocandidato/novocandidato.component';
import { AppComponent } from './app.component';
import { ConsultacandidatoComponent } from './consultacandidato/consultacandidato/consultacandidato.component'
import { HomeComponent } from './home/home/home.component';
import { ConsultavagaComponent } from './vagas/consultavaga/consultavaga/consultavaga.component';
import {NovavagaComponent} from './vagas/novavaga/novavaga/novavaga.component';
import {ConsultavagacandidatoComponent} from './vagacandidato/consultavagacandidato/consultavagacandidato/consultavagacandidato.component';
import {NovavagacandidatoComponent} from './vagacandidato/novavagacandidato/novavagacandidato/novavagacandidato.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'novocandidato', component: NovocandidatoComponent },
  { path: 'novocandidato/:id', component: NovocandidatoComponent },
  { path: 'consulta', component: ConsultacandidatoComponent },
  { path: 'consultavaga', component: ConsultavagaComponent },
  { path: 'consultavagacandidato', component: ConsultavagacandidatoComponent },
  { path: 'novavaga', component: NovavagaComponent },
  { path: 'novavaga/:id', component: NovavagaComponent },
  { path: 'novavagacandidato', component: NovavagacandidatoComponent },
  { path: 'novavagacandidato/:id', component: NovavagacandidatoComponent },
  { path: 'main', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
