export class CandidateJobDetail {
    id:number;
    idCandidate:number;
    idJob:number;
    nameCandidate:string;
    nameJob:string;
}