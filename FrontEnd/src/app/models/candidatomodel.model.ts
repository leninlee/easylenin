
export class Candidate {
    Id:number;
    name:string;
    surname:string;
    phone:string;
    email:string;
    coverletter:string;
    active:string;
    skype:string;
    linkedin:string;
    city:string;
    states:string;
    willingness:string;
    hourly:string;
}