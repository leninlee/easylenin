import { Component, OnInit } from '@angular/core';
import { Jobs } from '../../../models/vagamodel.model';
import { VagaserviceService} from '../../../servicos/vagaservice.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-novavaga',
  templateUrl: './novavaga.component.html',
  styleUrls: ['./novavaga.component.css']
})
export class NovavagaComponent implements OnInit {

  title:string;
  container:string;
  Id:number;
  public form:Jobs;

  constructor(private vagasService:VagaserviceService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.Id = this.activatedRoute.snapshot.params['id'];
    if(this.Id > 0){
      this.loadJob();
    }
  }

  loadJob(){
    this.vagasService.getById(this.Id).subscribe(data => {
      //console.log(data);
      console.log('Get data');
      this.form = <Jobs> data;
      this.title = this.form.title;
      this.container = this.form.container;

      //console.log(this.form.Id);
      console.log(this.Id);
      console.log(this.title);

    });
  }

  queryPosition(){
    window.location.href='/consultavaga';
  }

  savePosition(){
    console.log('saving position!');
    console.log(this.title);
    console.log(this.container);
    this.form = new Jobs();
    this.form.active = "A";
    this.form.container = this.container;
    this.form.title = this.title;
    
    if(this.Id==null){
      this.Id = 0;
    }
    console.log(this.Id);
    //this.form.Id = <number> this.Id;

    console.log(this.form);
    this.vagasService.saveJob(this.form,this.Id).subscribe(data => {
      //console.log(data);
      console.log('Saved data');
      window.location.href='/consultavaga';
    });
  }

}
