import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovavagaComponent } from './novavaga.component';

describe('NovavagaComponent', () => {
  let component: NovavagaComponent;
  let fixture: ComponentFixture<NovavagaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovavagaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovavagaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
