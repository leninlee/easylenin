import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultavagaComponent } from './consultavaga.component';

describe('ConsultavagaComponent', () => {
  let component: ConsultavagaComponent;
  let fixture: ComponentFixture<ConsultavagaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultavagaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultavagaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
