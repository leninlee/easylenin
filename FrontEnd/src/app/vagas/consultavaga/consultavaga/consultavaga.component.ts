import { Component, OnInit } from '@angular/core';
import { VagaserviceService } from '../../../servicos/vagaservice.service';

@Component({
  selector: 'app-consultavaga',
  templateUrl: './consultavaga.component.html',
  styleUrls: ['./consultavaga.component.css']
})
export class ConsultavagaComponent implements OnInit {

  positions:any = [];

  constructor(private vagasService:VagaserviceService) { }

  ngOnInit(): void {
    this.loadPositions();
  }

  loadPositions(){
    console.log('loadPositions-executed');
    //this.candidates = this.candidateService.getAll();
    this.vagasService.getAll().subscribe(data => {
      //console.log(data);
      this.positions = data;
      //console.log(data);
    });
  }

  newPosition(){
    window.location.href='/novavaga';
  }

  editPositions(event:any,item:any){
    console.log('Edit data 2');
    console.log(item);
    window.location.href="novavaga/" + item.id;
  }

  removePositions(event:any,item:any){
    console.log('Edit data 2');
    console.log(item);
    if(window.confirm('Confirma exclusão de registro?')){
      console.log('Confirma exclusao de registro');
      this.vagasService.removeJob(item.id).subscribe(data => {
        //this.candidates = data;
        alert('Registro excluído com sucesso!');
        window.location.href="/consultavaga";
      });
    }
  }

}
