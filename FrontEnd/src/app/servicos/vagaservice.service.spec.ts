import { TestBed } from '@angular/core/testing';

import { VagaserviceService } from './vagaservice.service';

describe('VagaserviceService', () => {
  let service: VagaserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VagaserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
