import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Jobs } from '../models/vagamodel.model';

@Injectable({
  providedIn: 'root'
})
export class VagaserviceService {
  url:string = `https://localhost:44353/api/Jobs`;

  constructor(private http: HttpClient) { }

  getAll() {
    console.log('access getAll()');
    return this.http.get(this.url);
  }

  getById(id:any) {
    console.log('access getById()-' + id);
    return this.http.get(this.url + '/' + id);
  }

  saveJob(job:Jobs,id:number){
    console.log('access saveJob()');
    if(id==0)
      return this.http.post(this.url,job);
    else
      return this.http.put(this.url + '/' + id,job);

  }

  removeJob(id:number){
    return this.http.delete(this.url + '/' + id);
  }

}
