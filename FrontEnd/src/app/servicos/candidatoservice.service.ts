import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Candidate } from '../models/candidatomodel.model';

@Injectable({
  providedIn: 'root'
})
export class CandidatoserviceService {
  url:string = `https://localhost:44353/api/Candidate`;

  constructor(private http: HttpClient) { }

  getAll() {
    console.log('access getAll()');
    return this.http.get(this.url);
  }

  getById(id:string) {
    console.log('access getById()-' + id);
    return this.http.get(this.url + '/' + id);
  }

  saveCandidate(candidate:Candidate,id:number){
    console.log('access saveCandidate()');
    if(id==0)
      return this.http.post(this.url,candidate);
    else
      return this.http.put(this.url + '/' + id,candidate);

  }

  removeCandidate(id:number){
    return this.http.delete(this.url + '/' + id);
  }

}
