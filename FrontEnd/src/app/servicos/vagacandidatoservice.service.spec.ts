import { TestBed } from '@angular/core/testing';

import { VagacandidatoserviceService } from './vagacandidatoservice.service';

describe('VagacandidatoserviceService', () => {
  let service: VagacandidatoserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VagacandidatoserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
