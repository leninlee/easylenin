import { TestBed } from '@angular/core/testing';

import { CandidatoserviceService } from './candidatoservice.service';

describe('CandidatoserviceService', () => {
  let service: CandidatoserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CandidatoserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
