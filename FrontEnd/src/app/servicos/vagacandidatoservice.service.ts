import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { CandidateJob } from '../models/vagacandidatomodel.model';

@Injectable({
  providedIn: 'root'
})
export class VagacandidatoserviceService {
  url:string = `https://localhost:44353/api/CandidateJob`;

  constructor(private http: HttpClient) { }

  getAll() {
    console.log('access getAll()');
    return this.http.get(this.url);
  }

  getById(id:any) {
    console.log('access getById()-' + id);
    return this.http.get(this.url + '/' + id);
  }

  saveCandidate(candidate:CandidateJob,id:number){
    console.log('access saveCandidate()');
    if(id==0)
      return this.http.post(this.url,candidate);
    else
      return this.http.put(this.url + '/' + id,candidate);

  }

  removeCandidate(id:number){
    return this.http.delete(this.url + '/' + id);
  }

}
