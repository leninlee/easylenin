import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultavagacandidatoComponent } from './consultavagacandidato.component';

describe('ConsultavagacandidatoComponent', () => {
  let component: ConsultavagacandidatoComponent;
  let fixture: ComponentFixture<ConsultavagacandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultavagacandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultavagacandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
