import { Component, OnInit } from '@angular/core';
import { VagacandidatoserviceService } from '../../../servicos/vagacandidatoservice.service';

@Component({
  selector: 'app-consultavagacandidato',
  templateUrl: './consultavagacandidato.component.html',
  styleUrls: ['./consultavagacandidato.component.css']
})
export class ConsultavagacandidatoComponent implements OnInit {

  candidatejob:any = [];

  constructor(private vagaCandidatoService:VagacandidatoserviceService) { }

  ngOnInit(): void {
    this.loadJobCandidate();
  }

  loadJobCandidate(){
    console.log('loadJobCandidate-executed');

    this.vagaCandidatoService.getAll().subscribe(data => {
      console.log(data);
      this.candidatejob = data;
      //console.log(data);
    });
  }

  newCandidate(){
    window.location.href='/novavagacandidato';
  }

  removeCandidates(event:any,item:any){
    console.log('Edit data 2');
    console.log(item);
    if(window.confirm('Confirma exclusão de registro?')){
      console.log('Confirma exclusao de registro');
      this.vagaCandidatoService.removeCandidate(item.id).subscribe(data => {
        //this.candidates = data;
        alert('Registro excluído com sucesso!');
        window.location.href="/consultavagacandidato";
      });
    }
  }

}
