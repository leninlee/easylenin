import { Component, OnInit } from '@angular/core';
import { CandidatoserviceService } from '../../../servicos/candidatoservice.service';
import { VagaserviceService } from '../../../servicos/vagaservice.service';
import {VagacandidatoserviceService } from '../../../servicos/vagacandidatoservice.service';
import { CandidateJob } from '../../../models/vagacandidatomodel.model';

@Component({
  selector: 'app-novavagacandidato',
  templateUrl: './novavagacandidato.component.html',
  styleUrls: ['./novavagacandidato.component.css']
})
export class NovavagacandidatoComponent implements OnInit {

  candidate:number;
  candidate_save:number;
  job:number;
  job_save:number;
  candidates:any = [];
  positions:any = [];
  form:CandidateJob;
  Id:number;

  constructor(private candidateService:CandidatoserviceService
    ,private vagasService:VagaserviceService,private vagaCandidatoService:VagacandidatoserviceService) { }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(){
    this.loadCandidates();
    this.loadPositions();
  }

  loadCandidates(){
    console.log('loadCandidates-executed');
    //this.candidates = this.candidateService.getAll();
    this.candidateService.getAll().subscribe(data => {
      //console.log(data);
      this.candidates = data;
      //console.log(data);
    });
  }

  loadPositions(){
    console.log('loadPositions-executed');
    //this.candidates = this.candidateService.getAll();
    this.vagasService.getAll().subscribe(data => {
      //console.log(data);
      this.positions = data;
      //console.log(data);
    });
  }

  queryCandidatePosition(){
    window.location.href='/consultavagacandidato';
  }

  selectCandidate(id:number){
    id = Number(id.toString().split(':')[0]);
    console.log('selected:' + id);
    this.candidate_save = id;
    //this.candidate = <number> this.candidate.toString().replace(": undefined","").toString();
  }

  selectPosition(id:any){
    id = Number(id.toString().split(':')[0]);
    console.log('selected:' + id);
    this.job_save = id;
  }

  saveCandidatePosition(){
    console.log('saving candidate vs position!');
    console.log(this.candidate_save);
    console.log(this.job_save);
    console.log('Process of saving');
    this.form = new CandidateJob();
    this.form.IdCandidate = this.candidate_save;
    this.form.IdJob = this.job_save;
    this.form.Id = 0;
    this.Id = 0;
    
    //console.log(this.form);
    this.vagaCandidatoService.saveCandidate(this.form,this.Id).subscribe(data => {
      //console.log(data);
      console.log('Saved data');
      window.location.href='/consultavagacandidato';
    });

  }

}
