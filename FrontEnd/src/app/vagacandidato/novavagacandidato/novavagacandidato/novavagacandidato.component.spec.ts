import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovavagacandidatoComponent } from './novavagacandidato.component';

describe('NovavagacandidatoComponent', () => {
  let component: NovavagacandidatoComponent;
  let fixture: ComponentFixture<NovavagacandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovavagacandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovavagacandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
